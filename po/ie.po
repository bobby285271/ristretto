# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Xfce
# This file is distributed under the same license as the xfce-apps.ristretto package.
# 
# Translators:
msgid ""
msgstr ""
"Project-Id-Version: Xfce Apps\n"
"Report-Msgid-Bugs-To: https://gitlab.xfce.org/\n"
"POT-Creation-Date: 2023-12-28 00:48+0100\n"
"PO-Revision-Date: 2013-07-03 18:30+0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: Interlingue (http://app.transifex.com/xfce/xfce-apps/language/ie/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ie\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: src/main.c:46
msgid "Version information"
msgstr "Information del version"

#: src/main.c:50
msgid "Start in fullscreen mode"
msgstr "Lansar in li mode plen-ecran"

#: src/main.c:54
msgid "Start a slideshow"
msgstr "Iniciar un exhibition"

#: src/main.c:62
msgid "Show settings dialog"
msgstr "Monstrar un dialog de parametres"

#: src/main.c:94
#, c-format
msgid ""
"%s: %s\n"
"\n"
"Try %s --help to see a full list of\n"
"available command line options.\n"
msgstr ""

#: src/main_window.c:39 org.xfce.ristretto.desktop.in:6
msgid "Image Viewer"
msgstr "Visor de images"

#: src/main_window.c:42
msgid "Could not save file"
msgstr "Ne successat gardar li file"

#: src/main_window.c:43
msgid "Some files could not be opened: see the text logs for details"
msgstr ""

#: src/main_window.c:44
#, c-format
msgid "An error occurred when deleting image '%s' from disk"
msgstr ""

#: src/main_window.c:45
#, c-format
msgid "An error occurred when sending image '%s' to trash"
msgstr ""

#: src/main_window.c:315
msgid "_File"
msgstr "_File"

#. Icon-name
#: src/main_window.c:321
msgid "_Open..."
msgstr "_Aperter..."

#. Label-text
#. Keyboard shortcut
#: src/main_window.c:323
msgid "Open an image"
msgstr "Aperter un image"

#. Icon-name
#: src/main_window.c:327
msgid "_Save copy..."
msgstr "_Gardar un copie..."

#. Label-text
#. Keyboard shortcut
#: src/main_window.c:329
msgid "Save a copy of the image"
msgstr "Gardar un copie del image"

#. Icon-name
#: src/main_window.c:333
msgid "Proper_ties..."
msgstr ""

#. Label-text
#. Keyboard shortcut
#: src/main_window.c:335
msgid "Show file properties"
msgstr "Monstrar proprietás del file"

#. Icon-name
#: src/main_window.c:339
msgid "_Print..."
msgstr "_Printar..."

#. Label-text
#. Keyboard shortcut
#: src/main_window.c:341
msgid "Print the current image"
msgstr ""

#. Icon-name
#: src/main_window.c:345 src/main_window.c:364
msgid "_Edit"
msgstr "_Redacter"

#. Label-text
#. Keyboard shortcut
#: src/main_window.c:347
msgid "Edit this image"
msgstr "Redacter ti-ci image"

#. Icon-name
#: src/main_window.c:351 src/preferences_dialog.c:218
#: src/properties_dialog.c:103
msgid "_Close"
msgstr "_Clúder"

#. Label-text
#. Keyboard shortcut
#: src/main_window.c:353
msgid "Close this image"
msgstr "Cluder ti-ci image"

#. Icon-name
#: src/main_window.c:357
msgid "_Quit"
msgstr "Sa_lir"

#. Label-text
#. Keyboard shortcut
#: src/main_window.c:359
msgid "Quit Ristretto"
msgstr "Salir Ristretto"

#: src/main_window.c:370
msgid "_Copy image to clipboard"
msgstr ""

#: src/main_window.c:376
msgid "_Open with"
msgstr "_Aperter per"

#: src/main_window.c:382
msgid "_Sort by"
msgstr ""

#. Icon-name
#: src/main_window.c:388
msgid "_Delete"
msgstr "_Deleter"

#. Label-text
#. Keyboard shortcut
#: src/main_window.c:390
msgid "Delete this image from disk"
msgstr "Deleter ti-ci image ex disco"

#. Icon-name
#: src/main_window.c:394
msgid "_Clear private data..."
msgstr ""

#. Icon-name
#: src/main_window.c:400
msgid "_Preferences..."
msgstr "_Preferenties..."

#: src/main_window.c:407
msgid "_View"
msgstr "_Vise"

#. Icon-name
#: src/main_window.c:413
msgid "_Fullscreen"
msgstr "Plen-_ecran"

#. Label-text
#. Keyboard shortcut
#: src/main_window.c:415
msgid "Switch to fullscreen"
msgstr "Mode plen-ecran"

#. Icon-name
#: src/main_window.c:419
msgid "_Leave Fullscreen"
msgstr "_Surtir plen-ecran"

#. Label-text
#. Keyboard shortcut
#: src/main_window.c:421
msgid "Leave Fullscreen"
msgstr "Surtir plen-ecran"

#. Icon-name
#: src/main_window.c:425
msgid "Set as _Wallpaper..."
msgstr "Assignar quam li _Tapete..."

#: src/main_window.c:432
msgid "_Zoom"
msgstr "_Scale"

#. Icon-name
#: src/main_window.c:438
msgid "Zoom _In"
msgstr "_Agrandar"

#. Label-text
#. Keyboard shortcut
#: src/main_window.c:440
msgid "Zoom in"
msgstr "Agrandar"

#. Icon-name
#: src/main_window.c:444
msgid "Zoom _Out"
msgstr "_Diminuer"

#. Label-text
#. Keyboard shortcut
#: src/main_window.c:446
msgid "Zoom out"
msgstr "Diminuer"

#. Icon-name
#: src/main_window.c:450 src/main_window.c:734
msgid "Zoom _Fit"
msgstr "_Ajustar"

#. Label-text
#. Keyboard shortcut
#: src/main_window.c:452
msgid "Zoom to fit window"
msgstr "Adjustar dimension"

#. Icon-name
#: src/main_window.c:456 src/main_window.c:740
msgid "_Normal Size"
msgstr "Origi_nal dimension"

#. Label-text
#. Keyboard shortcut
#: src/main_window.c:458
msgid "Zoom to 100%"
msgstr ""

#: src/main_window.c:463
msgid "_Default Zoom"
msgstr ""

#: src/main_window.c:470
msgid "_Rotation"
msgstr "_Rotation"

#. Icon-name
#: src/main_window.c:476
msgid "Rotate _Right"
msgstr "Rotar a _dextri"

#. Icon-name
#: src/main_window.c:482
msgid "Rotate _Left"
msgstr "Rotar a _levul"

#: src/main_window.c:489
msgid "_Flip"
msgstr "Re_flecter"

#: src/main_window.c:495
msgid "Flip _Horizontally"
msgstr "Reflecter _horizontalmen"

#: src/main_window.c:501
msgid "Flip _Vertically"
msgstr "Reflecter _verticalmen"

#: src/main_window.c:508
msgid "_Go"
msgstr "_Ear"

#. Icon-name
#: src/main_window.c:514
msgid "_Forward"
msgstr "_Avan"

#. Label-text
#. Keyboard shortcut
#: src/main_window.c:516
msgid "Next image"
msgstr "Sequent image"

#. Icon-name
#: src/main_window.c:520
msgid "_Back"
msgstr "_Retro"

#. Label-text
#. Keyboard shortcut
#: src/main_window.c:522
msgid "Previous image"
msgstr "Precedent image"

#. Icon-name
#: src/main_window.c:526
msgid "F_irst"
msgstr "Pr_im"

#. Label-text
#. Keyboard shortcut
#: src/main_window.c:528
msgid "First image"
msgstr "Prim image"

#. Icon-name
#: src/main_window.c:532
msgid "_Last"
msgstr "_Ultim"

#. Label-text
#. Keyboard shortcut
#: src/main_window.c:534
msgid "Last image"
msgstr "Ultim image"

#: src/main_window.c:539
msgid "_Help"
msgstr "Au_xilie"

#. Icon-name
#: src/main_window.c:545
msgid "_Contents"
msgstr "_Contenete"

#. Label-text
#. Keyboard shortcut
#: src/main_window.c:547
msgid "Display ristretto user manual"
msgstr "Monstrar li manuale de ristretto"

#. Icon-name
#: src/main_window.c:551
msgid "_About"
msgstr "_Pri"

#. Label-text
#. Keyboard shortcut
#: src/main_window.c:553
msgid "Display information about ristretto"
msgstr "Monstrar information pri ristretto"

#: src/main_window.c:558
msgid "_Position"
msgstr "_Position"

#: src/main_window.c:564
msgid "_Size"
msgstr "_Dimension"

#: src/main_window.c:570
msgid "Thumbnail Bar _Position"
msgstr "_Position del panel de miniaturas"

#: src/main_window.c:576
msgid "Thumb_nail Size"
msgstr "Dimension de _miniaturas"

#. Icon-name
#: src/main_window.c:583
msgid "Leave _Fullscreen"
msgstr "_Surtir plen-ecran"

#. Icon-name
#: src/main_window.c:601
msgid "_Show Toolbar"
msgstr "Monstrar li panel de _instrumentarium"

#. Icon-name
#: src/main_window.c:609
msgid "Show _Thumbnail Bar"
msgstr "_Monstrar li panel de miniaturas"

#. Icon-name
#: src/main_window.c:617
msgid "Show Status _Bar"
msgstr "Monstrar li panel del _statu"

#. Icon-name
#: src/main_window.c:629
msgid "file name"
msgstr ""

#. Icon-name
#: src/main_window.c:635
msgid "file type"
msgstr ""

#. Icon-name
#: src/main_window.c:641
msgid "date"
msgstr ""

#. Icon-name
#: src/main_window.c:652
msgid "Left"
msgstr "A levul"

#: src/main_window.c:658
msgid "Right"
msgstr "A dextri"

#: src/main_window.c:664
msgid "Top"
msgstr "Ad alt"

#: src/main_window.c:670
msgid "Bottom"
msgstr "A bass"

#: src/main_window.c:681
msgid "Very Small"
msgstr "Micrissim"

#: src/main_window.c:687
msgid "Smaller"
msgstr "Plu micri"

#: src/main_window.c:693
msgid "Small"
msgstr "Micri"

#: src/main_window.c:699
msgid "Normal"
msgstr "Normal"

#: src/main_window.c:705
msgid "Large"
msgstr "Grand"

#: src/main_window.c:711
msgid "Larger"
msgstr "Plu grand"

#: src/main_window.c:717
msgid "Very Large"
msgstr "Grandissim"

#. Icon-name
#: src/main_window.c:728
msgid "_Smart Zoom"
msgstr ""

#: src/main_window.c:874
msgid "Images"
msgstr "Images"

#. Create Play/Pause Slideshow actions
#: src/main_window.c:947
msgid "_Play"
msgstr "Re_producter"

#: src/main_window.c:947
msgid "Play slideshow"
msgstr "Iniciar un exhibition"

#: src/main_window.c:948
msgid "_Pause"
msgstr "_Pausar"

#: src/main_window.c:948
msgid "Pause slideshow"
msgstr "Pausar li exhibition"

#. Create Recently used items Action
#: src/main_window.c:951
msgid "_Recently used"
msgstr "_Recentmen usat"

#: src/main_window.c:951
msgid "Recently used"
msgstr "Recentmen usat"

#: src/main_window.c:1075 src/main_window.c:1631
msgid "Press open to select an image"
msgstr "Ha clic sur Aperter por selecter un image"

#: src/main_window.c:1573 src/main_window.c:1578
msgid "Open With Other _Application..."
msgstr "Aperter per altri _application..."

#: src/main_window.c:1588 src/main_window.c:1596
msgid "Empty"
msgstr "Vacui"

#: src/main_window.c:1636
msgid "Loading..."
msgstr "Carga..."

#: src/main_window.c:2286
msgid "Choose 'set wallpaper' method"
msgstr ""

#: src/main_window.c:2290 src/main_window.c:3419 src/main_window.c:3507
#: src/main_window.c:4185 src/privacy_dialog.c:149
#: src/xfce_wallpaper_manager.c:276 src/gnome_wallpaper_manager.c:190
msgid "_Cancel"
msgstr "_Anullar"

#: src/main_window.c:2293 src/main_window.c:4188
#: src/xfce_wallpaper_manager.c:282 src/gnome_wallpaper_manager.c:196
msgid "_OK"
msgstr "_OK"

#: src/main_window.c:2303 src/preferences_dialog.c:436
msgid ""
"Configure which system is currently managing your desktop.\n"
"This setting determines the method <i>Ristretto</i> will use\n"
"to configure the desktop wallpaper."
msgstr ""

#: src/main_window.c:2329 src/preferences_dialog.c:448
msgid "None"
msgstr "Null"

#: src/main_window.c:2333 src/preferences_dialog.c:452
msgid "Xfce"
msgstr "Xfce"

#: src/main_window.c:2337 src/preferences_dialog.c:456
msgid "GNOME"
msgstr "GNOME"

#: src/main_window.c:2852
msgid "Developers:"
msgstr ""

#: src/main_window.c:2864 org.xfce.ristretto.appdata.xml.in:11
msgid "Ristretto is an image viewer for the Xfce desktop environment."
msgstr "Ristretto es un visor de images por li ambientie Xfce."

#: src/main_window.c:2872
msgid "translator-credits"
msgstr "OIS <mistresssilvara@hotmail.com>, 2016"

#: src/main_window.c:3417
msgid "Open image"
msgstr "Aperter un image"

#: src/main_window.c:3419
msgid "_Open"
msgstr "_Aperter"

#: src/main_window.c:3437
msgid ".jp(e)g"
msgstr ".jp(e)g"

#: src/main_window.c:3442
msgid "All Files"
msgstr "Omni files"

#: src/main_window.c:3504
msgid "Save copy"
msgstr "Salvar un copie"

#: src/main_window.c:3508
msgid "_Save"
msgstr "_Gardar"

#: src/main_window.c:3617
msgid "Failed to print the image"
msgstr ""

#: src/main_window.c:3707
#, c-format
msgid "Are you sure you want to send image '%s' to trash?"
msgstr "Esque vu vole mover li image «%s» in li Paper-corb?"

#: src/main_window.c:3711
#, c-format
msgid "Are you sure you want to delete image '%s' from disk?"
msgstr ""

#: src/main_window.c:3723
msgid "_Do not ask again for this session"
msgstr "_Ne questionar denov in ti session"

#: src/main_window.c:4180
msgid "Edit with"
msgstr "Redacter per"

#: src/main_window.c:4201
#, c-format
msgid "Open %s and other files of type %s with:"
msgstr "Aperter «%s» e altri files de tip «%s» per:"

#: src/main_window.c:4207
msgid "Use as _default for this kind of file"
msgstr "Usar quam pre_definit por ti-ci tip de files"

#: src/main_window.c:4292
msgid "Recommended Applications"
msgstr "Recomandat applicationes"

#: src/main_window.c:4341
msgid "Other Applications"
msgstr "Altri applicationes"

#: src/icon_bar.c:250
msgid "Orientation"
msgstr "Orientation"

#: src/icon_bar.c:251
msgid "The orientation of the iconbar"
msgstr ""

#: src/icon_bar.c:264
msgid "Icon Bar Model"
msgstr ""

#: src/icon_bar.c:265
msgid "Model for the icon bar"
msgstr ""

#: src/icon_bar.c:281
msgid "Active"
msgstr "Activ"

#: src/icon_bar.c:282
msgid "Active item index"
msgstr ""

#: src/icon_bar.c:298 src/icon_bar.c:299
msgid "Show Text"
msgstr "Monstrar li textu"

#: src/icon_bar.c:311
msgid "Scrolled window"
msgstr ""

#: src/icon_bar.c:312
msgid "Scrolled window icon bar is placed into"
msgstr ""

#: src/icon_bar.c:318 src/icon_bar.c:319
msgid "Active item fill color"
msgstr ""

#: src/icon_bar.c:325 src/icon_bar.c:326
msgid "Active item border color"
msgstr ""

#: src/icon_bar.c:332 src/icon_bar.c:333
msgid "Active item text color"
msgstr ""

#: src/icon_bar.c:339 src/icon_bar.c:340
msgid "Cursor item fill color"
msgstr ""

#: src/icon_bar.c:346 src/icon_bar.c:347
msgid "Cursor item border color"
msgstr ""

#: src/icon_bar.c:353 src/icon_bar.c:354
msgid "Cursor item text color"
msgstr ""

#: src/privacy_dialog.c:119
msgid "Time range to clear:"
msgstr ""

#: src/privacy_dialog.c:122
msgid "Cleanup"
msgstr ""

#: src/privacy_dialog.c:125
msgid "Last Hour"
msgstr "Li ultim hor"

#: src/privacy_dialog.c:126
msgid "Last Two Hours"
msgstr "Li ultim du hores"

#: src/privacy_dialog.c:127
msgid "Last Four Hours"
msgstr "Li ultim quar hores"

#: src/privacy_dialog.c:128
msgid "Today"
msgstr "Hodie"

#: src/privacy_dialog.c:129
msgid "Everything"
msgstr "Omnicos"

#: src/privacy_dialog.c:152 src/xfce_wallpaper_manager.c:279
#: src/gnome_wallpaper_manager.c:193
msgid "_Apply"
msgstr "_Applicar"

#: src/privacy_dialog.c:438
msgid "Clear private data"
msgstr ""

#: src/preferences_dialog.c:249
msgid "Display"
msgstr "Monstrar"

#: src/preferences_dialog.c:256
msgid "Background color"
msgstr "Color del funde"

#: src/preferences_dialog.c:260
msgid "Override background color:"
msgstr ""

#: src/preferences_dialog.c:288
msgid "Quality"
msgstr "Qualitá"

#: src/preferences_dialog.c:292
msgid ""
"With this option enabled, the maximum image-quality will be limited to the "
"screen-size."
msgstr ""

#: src/preferences_dialog.c:296
msgid "Limit rendering quality"
msgstr ""

#: src/preferences_dialog.c:308
msgid "Fullscreen"
msgstr "Plen-ecran"

#: src/preferences_dialog.c:312
msgid "Thumbnails"
msgstr "Miniaturas"

#: src/preferences_dialog.c:315
msgid ""
"The thumbnail bar can be automatically hidden when the window is fullscreen."
msgstr ""

#: src/preferences_dialog.c:319
msgid "Hide thumbnail bar when fullscreen"
msgstr ""

#: src/preferences_dialog.c:327
msgid "Clock"
msgstr "Horloge"

#: src/preferences_dialog.c:330
msgid ""
"Show an analog clock that displays the current time when the window is "
"fullscreen"
msgstr ""

#: src/preferences_dialog.c:334
msgid "Show Fullscreen Clock"
msgstr ""

#: src/preferences_dialog.c:340
msgid "Mouse cursor"
msgstr ""

#: src/preferences_dialog.c:343
msgid ""
"The mouse cursor can be automatically hidden after a certain period of inactivity\n"
"when the window is fullscreen."
msgstr ""

#: src/preferences_dialog.c:350
msgid "Period of inactivity (seconds):"
msgstr ""

#: src/preferences_dialog.c:369
msgid "Slideshow"
msgstr "Exhibition"

#: src/preferences_dialog.c:373
msgid "Timeout"
msgstr ""

#: src/preferences_dialog.c:376
msgid ""
"The time period an individual image is displayed during a slideshow\n"
"(in seconds)"
msgstr ""

#: src/preferences_dialog.c:390
msgid "Control"
msgstr "Control"

#: src/preferences_dialog.c:394
msgid "Scroll wheel"
msgstr "Color-rote"

#: src/preferences_dialog.c:397
msgid "Invert zoom direction"
msgstr "Inverter li scale"

#: src/preferences_dialog.c:408
msgid "Behaviour"
msgstr "Conduida"

#: src/preferences_dialog.c:412
msgid "Startup"
msgstr "Al inicie"

#: src/preferences_dialog.c:414
msgid "Maximize window on startup when opening an image"
msgstr ""

#: src/preferences_dialog.c:420
msgid "Wrap around images"
msgstr ""

#: src/preferences_dialog.c:431
msgid "Desktop"
msgstr "Pupitre"

#: src/preferences_dialog.c:538
msgid "Image Viewer Preferences"
msgstr "Preferenties de visor de images"

#: src/properties_dialog.c:138
msgid "<b>Name:</b>"
msgstr "<b>Nómine:</b>"

#: src/properties_dialog.c:139
msgid "<b>Kind:</b>"
msgstr "<b>Tip:</b>"

#: src/properties_dialog.c:140
msgid "<b>Modified:</b>"
msgstr "<b>Modificat:</b>"

#: src/properties_dialog.c:141
msgid "<b>Accessed:</b>"
msgstr "<b>Acesset:</b>"

#: src/properties_dialog.c:142
msgid "<b>Size:</b>"
msgstr "<b>Grandore:</b>"

#: src/properties_dialog.c:166
msgid "General"
msgstr "General"

#: src/properties_dialog.c:172
msgid "Image"
msgstr "Image"

#: src/properties_dialog.c:391
msgid "<b>Date taken:</b>"
msgstr "<b>Fat:</b>"

#: src/properties_dialog.c:403 src/properties_dialog.c:415
#: src/properties_dialog.c:427
#, c-format
msgid "<b>%s</b>"
msgstr "<b>%s</b>"

#: src/properties_dialog.c:486
#, c-format
msgid "%s - Properties"
msgstr "Proprietás de %s"

#: src/image_list.c:32
msgid "No supported image type found in the directory"
msgstr ""

#: src/image_list.c:33
msgid "Directory only partially loaded"
msgstr ""

#: src/image_list.c:34
msgid "Could not load directory"
msgstr ""

#: src/image_list.c:35
msgid "Unsupported mime type"
msgstr ""

#: src/thumbnailer.c:362
msgid ""
"The thumbnailer-service can not be reached,\n"
"for this reason, the thumbnails can not be\n"
"created.\n"
"\n"
"Install <b>Tumbler</b> or another <i>thumbnailing daemon</i>\n"
"to resolve this issue."
msgstr ""

#: src/thumbnailer.c:371
msgid "Do _not show this message again"
msgstr "_Ne monstrar plu ti-ci avise"

#: src/xfce_wallpaper_manager.c:254 src/gnome_wallpaper_manager.c:176
msgid "Style:"
msgstr "Stil:"

#: src/xfce_wallpaper_manager.c:266
msgid "Apply to all workspaces"
msgstr "Applicar a omni labor-spacies"

#: src/xfce_wallpaper_manager.c:274 src/gnome_wallpaper_manager.c:188
msgid "Set as wallpaper"
msgstr "Assignar quam li Tapete"

#: src/xfce_wallpaper_manager.c:336 src/gnome_wallpaper_manager.c:246
msgid "Auto"
msgstr "Auto"

#: src/xfce_wallpaper_manager.c:339
msgid "Centered"
msgstr "Centrat"

#: src/xfce_wallpaper_manager.c:342
msgid "Tiled"
msgstr "Tegulat"

#: src/xfce_wallpaper_manager.c:345
msgid "Stretched"
msgstr "Extendet"

#: src/xfce_wallpaper_manager.c:348
msgid "Scaled"
msgstr "Scalat"

#: src/xfce_wallpaper_manager.c:351
msgid "Zoomed"
msgstr "Agrandat"

#: org.xfce.ristretto.desktop.in:4
msgid "Ristretto Image Viewer"
msgstr "Visor de images Ristretto"

#: org.xfce.ristretto.desktop.in:5
msgid "Look at your images easily"
msgstr "Vise vor images facilmen"

#: org.xfce.ristretto.appdata.xml.in:7
msgid "Ristretto"
msgstr ""

#: org.xfce.ristretto.appdata.xml.in:8
msgid "Fast and lightweight image viewer"
msgstr ""

#: org.xfce.ristretto.appdata.xml.in:13
msgid ""
"The Ristretto Image Viewer is an application that can be used to view and "
"scroll through images, run a slideshow of images, open images with other "
"applications like an image-editor or configure an image as the desktop "
"wallpaper."
msgstr ""

#: org.xfce.ristretto.appdata.xml.in:34
msgid "Xfce Developers"
msgstr ""
